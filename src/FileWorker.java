import com.sun.istack.internal.NotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileWorker {
    private static final String DIRECTORY_PATH = "src/files/";

    public static final String INPUT_FILE = DIRECTORY_PATH + "lab_7.txt";
    public static final String OUTPUT_FILE = DIRECTORY_PATH + "result.txt";

    public static void write(@NotNull String fileName, @NotNull List<Integer> integers) {
        if (integers.isEmpty()){
            throw new RuntimeException("Collection is empty");
        }

        write(fileName, convertIntegersToString(integers));
    }

    public static void write(@NotNull String fileName, @NotNull String text) {
        //Оприділяємо файл
        final File file = new File(fileName);

        try {
            //Перевіряємо, якщо файл не існує то створюємо його;
            if(!file.exists()){
                if(!file.createNewFile()){
                    throw new RuntimeException("Сan not create file: " + fileName);
                }
            }

            PrintWriter out = null;

            try {
                //PrintWriter дає моживість записувати дані у файл;
                out = new PrintWriter(file.getAbsoluteFile());

                //Записуємо текст у файл
                out.print(text);
            } finally {
                //Після чого ми повинні закрити файл
                if (out != null){
                    out.close();
                }
            }
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Integer> read(@NotNull String fileName) throws FileNotFoundException {
        File file = new File(fileName);
        if (!file.exists()){
            throw new FileNotFoundException(file.getName());
        }

        final List<Integer> result = new ArrayList<>();

        final Scanner scanner = new Scanner(file);

        while(scanner.hasNextInt()){
            result.add(scanner.nextInt());
        }

        return result;
    }

    private static String convertIntegersToString(List<Integer> integers) {
        String result = "";

        for (Integer integer :
                integers) {
            result += integer + " ";
        }

        return result;
    }
}