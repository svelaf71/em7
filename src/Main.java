import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Main {
    private static final double alpha = 0.1;

    public static void main(String[] args) {
        String result = "";

        //Генерація нормального розподілу та запис вибірки у файл
        FileWorker.write(FileWorker.INPUT_FILE, Algo.generateNormalDistribution(50));

        //Зчитування вибірки з файлу
        ArrayList<Integer> normalDistribution = null;
        try {
            normalDistribution = (ArrayList<Integer>) FileWorker.read(FileWorker.INPUT_FILE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        //Запис вибірки у файл з результатом
        result += "Вихідна вибірка:\n" + normalDistribution + "\n";

        ArrayList<Double> tEstimated = (ArrayList<Double>) Algo.generateTEstimated(normalDistribution);

        double tA = Algo.generateTA(alpha, normalDistribution.size());

        for (Double tE:
                tEstimated) {
            if (tE > tA) {
                result += "Значення: " + normalDistribution.get(tEstimated.indexOf(tE));
                result += "; Груба помилка: " + tE + "\n";
            }
        }

        System.out.println("\n\n\n\n");
        int min = Algo.calculateMin(normalDistribution);

        //SKV < 0.3
        int SKV = Algo.calculateMax(normalDistribution) + 1;
        normalDistribution.set(normalDistribution.indexOf(min), SKV);
        tEstimated.clear();
        tEstimated = (ArrayList<Double>) Algo.generateTEstimated(normalDistribution);

        result += "\n\nЗмінюємо мінімальне значення у вибірці на: " + SKV + "\n";

        for (Double tE:
                tEstimated) {
            if (tE > tA) {
                result += "Значення: " + normalDistribution.get(tEstimated.indexOf(tE));
                result += "; Груба помилка: " + tE + "\n";
            }
        }

        //SKV < 1
        int SKV1 = Algo.calculateMax(normalDistribution) + 3;
        normalDistribution.set(normalDistribution.indexOf(SKV), SKV1);
        tEstimated.clear();
        tEstimated = (ArrayList<Double>) Algo.generateTEstimated(normalDistribution);

        result += "\n\nЗмінюємо мінімальне значення у вибірці на: " + SKV1 + "\n";

        for (Double tE:
                tEstimated) {
            if (tE > tA) {
                result += "Значення: " + normalDistribution.get(tEstimated.indexOf(tE));
                result += "; Груба помилка: " + tE + "\n";
            }
        }

        //SKV випадає
        int SKV2 = Algo.calculateMax(normalDistribution) + 10;
        normalDistribution.set(normalDistribution.indexOf(SKV1), SKV2);
        tEstimated.clear();
        tEstimated = (ArrayList<Double>) Algo.generateTEstimated(normalDistribution);

        result += "\n\nЗмінюємо мінімальне значення у вибірці на: " + SKV2 + "\n";

        for (Double tE:
                tEstimated) {
            if (tE > tA) {
                result += "Значення: " + normalDistribution.get(tEstimated.indexOf(tE));
                result += "; Груба помилка: " + tE + "\n";
            }
        }

        //Запис результатів у файл
        FileWorker.write(FileWorker.OUTPUT_FILE, result);
    }
}
