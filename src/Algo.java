import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class Algo {
    //Генерація нормального розподілу
    static List<Integer> generateNormalDistribution(final int size){
        final List<Integer> result = new ArrayList<>();
        final Random random = new Random();

        final double variance = 2;
        final double mean = 5;

        for (int i = 0; i < size; i++){
            result.add((int) (mean + random.nextGaussian() * variance));
        }

        return result;
    }

    //Пошук t розрахункового:
    static List<Double> generateTEstimated(List<Integer> integers) {
        ArrayList<Double> tEstimated = new ArrayList<>();
        double mean = calculateMean(integers);
        double standartDeviation = calculateStandartDeviation(integers);

        for (int i = 0; i < integers.size(); i++) {
            tEstimated.add((integers.get(i) - mean) / standartDeviation);
        }

        return tEstimated;
    }

    static double generateTA(double alpha, int size) {
        double tA = 0.3053 * Math.log10(size) + 1.6513;

        return tA;
    }

    //Пошук середнього квадратичного
    static double calculateStandartDeviation(List<Integer> integers) {
        return Math.sqrt(calculateVariance(integers));
    }

    //Розрахунок дисперсії
    public static double calculateVariance(List<Integer> integers) {
        double mean = calculateMean(integers);
        double sum = 0.0;

        for (Integer integer:
                integers) {
            sum += (integer - mean) * (integer - mean);
        }

        return sum / integers.size();
    }

    //Пошук середнього значення у вибірці
    static double calculateMean(List<Integer> arr) {
        double sum = 0;

        for (int a:
                arr) {
            sum += a;
        }

        return sum / arr.size();
    }

    //Пошук мінімального значення
    static int calculateMin(List<Integer> integers){
        int min = integers.get(0);

        for (Integer integer :
                integers) {
            if (integer < min) {
                min = integer;
            }
        }

        return min;
    }

    //Пошук максимального значення
    static int calculateMax(List<Integer> integers){
        int max = integers.get(0);

        for (Integer integer :
                integers) {
            if (integer > max) {
                max = integer;
            }
        }

        return max;
    }
}
